package com.example.crcompany.APIs;

import com.example.crcompany.ModelClass.City;
import com.example.crcompany.ModelClass.Company;
import com.example.crcompany.ModelClass.Getrequests;
import com.example.crcompany.ModelClass.Result;
import com.example.crcompany.ModelClass.State;
import com.example.crcompany.ModelClass.StudentApplications;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface ApiService {

    @FormUrlEncoded
    @POST("login.php")
    Call<Result> comLogin(
            @Field("user_type") String user_type,
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("state.php")
    Call<State> getstate(
            @Field("user_type") String user_type
    );

    @FormUrlEncoded
    @POST("city.php")
    Call<City> getcity(
            @Field("S") String S
    );

//    @FormUrlEncoded
//    @POST("Registration.php")
//    Call<Result> comRegistration(
//            @Field("user_type") String user_type,
//            @Field("cname") String cname,
//            @Field("email") String email,
//            @Field("mobile") String mobile,
//            @Field("address") String address,
//            @Field("state") String state,
//            @Field("city") String city,
//            @Field("website") String website,
//            @Field("password") String password
//    );

    @POST("Registration.php")
    Call<Result> studentRegistration(@Body RequestBody profile_image);

    @FormUrlEncoded
    @POST("getprofile.php")
    Call<Company> getcompany(
            @Field("user_type") String user_type,
            @Field("comID") String comID
    );

//    @FormUrlEncoded
//    @POST("updateprofile.php")
//    Call<Result> updateprofile(
//            @Field("user_type") String user_type,
//            @Field("comID") String comID,
//            @Field("cname") String cname,
//            @Field("email") String email,
//            @Field("mobile") String mobile,
//            @Field("address") String address,
//            @Field("state") String state,
//            @Field("city") String city,
//             @Field("website") String website
//
//    );

    @Multipart
    @POST("updateprofile.php")
    Call<Result> updateprofile(@Part("user_type") RequestBody user_type,
                               @Part("comID") RequestBody comID,
                               @Part("cname") RequestBody cname,
                               @Part("email") RequestBody email,
                               @Part("mobile") RequestBody mobile,
                               @Part("address") RequestBody address,
                               @Part("state") RequestBody state,
                               @Part("city") RequestBody city,
                               @Part("website") RequestBody website,
                               @Part MultipartBody.Part profile_image
    );

    @FormUrlEncoded
    @POST("changepassword.php")
    Call<Result> changepassword(
            @Field("user_type") String user_type,
            @Field("comID") String comID,
            @Field("oldpass") String oldpass,
            @Field("newpass") String newpass
    );


    @FormUrlEncoded
    @POST("comrequest.php")
    Call<Result> comMakereq(
            @Field("comID") String comID,
            @Field("comname") String comname,
            @Field("place_interview") String place_interview,
            @Field("requested_date") String requested_date,
            @Field("interview_date") String interview_date,
            @Field("email") String email,
            @Field("mobile") String mobile,
            @Field("language") String language,
            @Field("website") String website,
            @Field("other_details") String other_details
    );

    @FormUrlEncoded
    @POST("getcomrequest.php")
    Call<Getrequests> comGetreqs(
            @Field("user_type") String user_type,
            @Field("comID") String comID
    );

    @FormUrlEncoded
    @POST("getstudentapplication.php")
    Call<StudentApplications> getstudentapplication(
            @Field("user_type") String user_type,
            @Field("cr_id") String cr_id
    );

    @Streaming
    @GET
    Call<ResponseBody> downloadFileWithDynamicUrlAsync(@Url String fileUrl);

}
