package com.example.crcompany;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.crcompany.APIs.Api;
import com.example.crcompany.APIs.ApiService;
import com.example.crcompany.ModelClass.Result;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Login extends AppCompatActivity {

    EditText username_et,password_et;
    Button login_btn;
    TextView signup_tv;
    SharedPreferences sharedPreferences_for_login;
    String myprefe2="login_shared_pre",comID="ID",comemail="Email",compass="Password",comname="comname",user_type="company",email,password;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
        if (sharedPreferences_for_login.contains(comemail)&&sharedPreferences_for_login.contains(compass))
        {
            Intent intent=new Intent(Login.this,Company_dashboard.class);
            startActivity(intent);
            finish();
        }

        username_et=findViewById(R.id.login_username);
        password_et=findViewById(R.id.login_password);
        login_btn=findViewById(R.id.login_login_btn);
        signup_tv=findViewById(R.id.login_tv_sign_up);

        signup_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Login.this,Registration.class);
                startActivity(intent);
            }
        });
    }

    public void onclick1(View v)
    {


        if (username_et.getText().toString().equals(""))
        {
            username_et.setError("enter your id");
        }
        else if (password_et.getText().toString().equals(""))
        {
            password_et.setError("enter your password");
        }
        else
        {
            progressDialog = new ProgressDialog(Login.this);
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            email=username_et.getText().toString();
            password=password_et.getText().toString();

            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(Api.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            ApiService apiService=retrofit.create(ApiService.class);

            Call<Result> call=apiService.comLogin(user_type,email,password);

            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    progressDialog.dismiss();
                    if (response.body()!=null)
                    {
                        if (response.body().getSuccess())
                        {
                            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
                            editor2.putString(comemail,email);
                            editor2.putString(compass,password);
                            editor2.putString(comID,response.body().getID().toString());
                            editor2.putString(comname,response.body().getCompany_name());
                            editor2.commit();
                            Intent intent=new Intent(Login.this,Company_dashboard.class);
                            startActivity(intent);
                            finish();
                        }
                        else
                        {
                            Toast.makeText(Login.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(Login.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
//                    Log.d("Loginmessage","iss>>"+t.getMessage());
                    progressDialog.dismiss();
                    Toast.makeText(Login.this,t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }

    }


}

