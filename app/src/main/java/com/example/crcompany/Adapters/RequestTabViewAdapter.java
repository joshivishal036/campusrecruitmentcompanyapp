package com.example.crcompany.Adapters;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import com.example.crcompany.FragmentOutdatedRequest;
import com.example.crcompany.FragmentUpcomingRequests;
import com.example.crcompany.Requests;

public class RequestTabViewAdapter extends FragmentPagerAdapter {
    private Context context;
    public int tabcount;
    SharedPreferences sharedPreferences_for_login;
    Requests requests;

    public RequestTabViewAdapter(Requests requests, @NonNull FragmentManager fm, int tabcount, SharedPreferences sharedPreferences_for_login) {
        super(fm);
        this.context= requests;
        this.tabcount=tabcount;
        this.sharedPreferences_for_login=sharedPreferences_for_login;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position)
        {
            case 0:
                FragmentUpcomingRequests fragmentUpcomingRequests=new FragmentUpcomingRequests(this.sharedPreferences_for_login);
                return fragmentUpcomingRequests;

            case 1:
                FragmentOutdatedRequest fragmentOutdatedRequest=new FragmentOutdatedRequest(this.sharedPreferences_for_login);
                return fragmentOutdatedRequest;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabcount;
    }
}
