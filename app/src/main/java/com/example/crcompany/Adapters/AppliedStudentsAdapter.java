package com.example.crcompany.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.crcompany.APIs.Api;
import com.example.crcompany.ModelClass.StudentApplicationsDetails;
import com.example.crcompany.R;
import com.example.crcompany.RequestDetails;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AppliedStudentsAdapter extends RecyclerView.Adapter<AppliedStudentsAdapter.ViewHolder> {
    //    ArrayList<String> interview;
//    ArrayList<String> requested;
//    ArrayList<String> language;
    ArrayList<StudentApplicationsDetails> student_details_array;
    Context context;
    Adapterclick adapterclick;


    public AppliedStudentsAdapter(RequestDetails requestDetails, ArrayList<StudentApplicationsDetails> request_details_array) {
        this.context = requestDetails;

        this.student_details_array = request_details_array;

        try {
            this.adapterclick = (Adapterclick) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activty must implement adaptercallback");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listview = layoutInflater.inflate(R.layout.applied_students_recyclerlist, parent, false);
        ViewHolder viewHolder = new ViewHolder(listview);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.name_tv.setText(student_details_array.get(position).getStud_app_stud_name());
        holder.email_tv.setText(student_details_array.get(position).getStud_app_stud_email());
        Glide.with(context)
                .load(Api.BASE_URL+student_details_array.get(position).getStud_app_stud_profileimage_url())
                .thumbnail(0.5f)
                .apply(RequestOptions.placeholderOf(R.drawable.profile_image)
                        .error(R.drawable.login_icon)
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return student_details_array.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name_tv;
        public TextView email_tv;
        public CircleImageView imageView;

        public ViewHolder(View list) {
            super(list);
            this.name_tv = list.findViewById(R.id.applied_stud_recyclerlist_name_tv);
            this.email_tv = list.findViewById(R.id.applied_stud_recyclerlist_mail_tv);
            this.imageView=list.findViewById(R.id.applied_stud_recyclerlist_profile_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                   Log.d("joshivishal", "isss: "+getAdapterPosition());
                    adapterclick.recyclerviewclick(getAdapterPosition());
                }
            });
        }
    }

    public interface Adapterclick {
        void recyclerviewclick(int position);
    }
}

