package com.example.crcompany.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.crcompany.FragmentOutdatedRequest;
import com.example.crcompany.FragmentUpcomingRequests;
import com.example.crcompany.ModelClass.GetRequestsSub;
import com.example.crcompany.R;

import java.util.ArrayList;

public class OutdatedRequestRecyclerAdapter extends RecyclerView.Adapter<OutdatedRequestRecyclerAdapter.ViewHolder> {
    //    ArrayList<String> interview;
//    ArrayList<String> requested;
//    ArrayList<String> language;
    ArrayList<GetRequestsSub> request_details_array;
    FragmentOutdatedRequest fragmentOutdatedRequest;
    Adapterclick adapterclick;


    public OutdatedRequestRecyclerAdapter(FragmentOutdatedRequest fragmentOutdatedRequest, ArrayList<GetRequestsSub> request_details_array) {
        this.fragmentOutdatedRequest = fragmentOutdatedRequest;

        this.request_details_array = request_details_array;

        try {
            this.adapterclick = (Adapterclick) fragmentOutdatedRequest;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activty must implement adaptercallback");
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listview = layoutInflater.inflate(R.layout.requests_recyclerlist, parent, false);
        ViewHolder viewHolder = new ViewHolder(listview);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.interview_tv.setText(request_details_array.get(position).getCr_interview_date());
        holder.requested_tv.setText(request_details_array.get(position).getCr_requested_date());
        holder.language_tv.setText(request_details_array.get(position).getCr_language());
        holder.location_tv.setText(request_details_array.get(position).getCr_interview_place());

    }

    @Override
    public int getItemCount() {
        return request_details_array.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView interview_tv;
        public TextView requested_tv;
        public TextView language_tv;
        public TextView location_tv;


        public ViewHolder(View list) {
            super(list);
            this.interview_tv = list.findViewById(R.id.req_rec_interviewdate_tv);
            this.requested_tv = list.findViewById(R.id.req_rec_requesteddate_tv);
            this.language_tv = list.findViewById(R.id.req_rec_language_tv);
            this.location_tv = list.findViewById(R.id.req_rec_location_tv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                   Log.d("joshivishal", "isss: "+getAdapterPosition());
                    adapterclick.recyclerviewclick(getAdapterPosition());
                }
            });
        }
    }

    public interface Adapterclick {
        void recyclerviewclick(int position);
    }
}

