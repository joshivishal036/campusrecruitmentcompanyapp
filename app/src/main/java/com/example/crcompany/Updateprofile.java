package com.example.crcompany;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.crcompany.APIs.Api;
import com.example.crcompany.APIs.ApiService;
import com.example.crcompany.ModelClass.City;
import com.example.crcompany.ModelClass.Company;
import com.example.crcompany.ModelClass.Result;
import com.example.crcompany.ModelClass.State;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Updateprofile extends AppCompatActivity {

    ImageView imageView_profile, imageView_pass;
    CircleImageView profile_imageview;
    EditText comname_et, email_et, mobile_et, address_et, state_et, website_et, old_pass_et, new_pass_et, comfirm_new_pass_et;
    Button update_data_btn, change_pass_btn;
    TextView email_tv, update_data_result_tv, change_pass_result_tv;
    Spinner city_sp,state_sp;
    ArrayList<String> strings_city = new ArrayList<>();
    ArrayAdapter<String> city_adapter;
    ArrayList<String> strings_state = new ArrayList<>();
    ArrayAdapter<String> state_adapter;
    Integer REQUEST_CODE=200;
    String filepath;
    String typedemail, emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String myprefe2 = "login_shared_pre", comID = "ID", comemail = "Email", compass = "Password", user_type = "company", cname, mobile, email, address, state, city, website;
    SharedPreferences sharedPreferences_for_login;
    ProgressDialog progressDialog;
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Api.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updateprofile);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
        profile_imageview=findViewById(R.id.update_pro_profile_image);

        profile_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,REQUEST_CODE);
            }
        });

        imageView_profile = findViewById(R.id.update_pro_data_icon_iv);
        imageView_profile.setImageResource(R.drawable.stud_dash_updatepro_icon);
        imageView_pass = findViewById(R.id.update_pro_change_pass_icon_iv);
        imageView_pass.setImageResource(R.drawable.update_pro_change_pass_icon);
        comname_et = findViewById(R.id.update_pro_comname_et);
        email_et = findViewById(R.id.update_pro_email_et);
        email_tv = findViewById(R.id.update_pro_email_tv);
        mobile_et = findViewById(R.id.update_pro_mobile_et);
        address_et = findViewById(R.id.update_pro_address_et);
        state_sp = findViewById(R.id.update_pro_state_sp);
        city_sp = findViewById(R.id.update_pro_city_sp);
        website_et = findViewById(R.id.update_pro_website_et);
        old_pass_et = findViewById(R.id.update_pro_old_pass_et);
        new_pass_et = findViewById(R.id.update_pro_new_pass_et);
        comfirm_new_pass_et = findViewById(R.id.update_pro_confirm_new_pass_et);
        update_data_btn = findViewById(R.id.update_pro_update_data_btn);
        update_data_result_tv = findViewById(R.id.update_pro_data_update_result_tv);
        change_pass_btn = findViewById(R.id.update_pro_change_pass_btn);
        change_pass_result_tv = findViewById(R.id.update_pro_change_pass_result_tv);

        strings_state.add("Select State");
        strings_city.add("Select City");

        setstate();
       state_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               setcity(state_sp.getSelectedItem().toString());
           }

           @Override
           public void onNothingSelected(AdapterView<?> adapterView) {
               setcity(state_sp.getSelectedItem().toString());
           }
       });

        email_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                typedemail = email_et.getText().toString().trim();
                if (typedemail.matches(emailPattern) && editable.length() > 0) {

                    email_tv.setText("valid Email");
                    email_tv.setTextColor(getResources().getColor(R.color.green));
                } else {

                    email_tv.setText("invalid Email");
                    email_tv.setTextColor(getResources().getColor(R.color.red));
                }
            }
        });

    }

    public void onclick1(View view) {

//        sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
//        progressDialog = new ProgressDialog(Updateprofile.this);
//        progressDialog.setMessage("Please Wait");
//        progressDialog.setCancelable(false);
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.show();

        cname = comname_et.getText().toString();
        email = email_et.getText().toString();
        mobile = mobile_et.getText().toString();
        address = address_et.getText().toString();
        state = state_sp.getSelectedItem().toString();
        city = city_sp.getSelectedItem().toString();
        website = website_et.getText().toString();

        updatedata(user_type,sharedPreferences_for_login.getString(comID,""),filepath,cname,email,mobile,address,state,city,website);


    }

    public void onclick2(View view) {

        if (new_pass_et.getText().toString().equals(comfirm_new_pass_et.getText().toString())) {
            sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            progressDialog = new ProgressDialog(Updateprofile.this);
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            String oldpass = old_pass_et.getText().toString();
            final String newpass = new_pass_et.getText().toString();

            ApiService service = retrofit.create(ApiService.class);
            Call<Result> call = service.changepassword(user_type, sharedPreferences_for_login.getString(comID, ""), oldpass, newpass);

            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    progressDialog.dismiss();
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor2 = sharedPreferences_for_login.edit();
                            editor2.putString(compass, newpass);
                            editor2.commit();
                            update_data_result_tv.setText("Data Updated Successfully!!");
                        } else {
                            Toast.makeText(Updateprofile.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Updateprofile.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(Updateprofile.this, "no response", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    public void set_company_details() {


        progressDialog = new ProgressDialog(Updateprofile.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();


        ApiService apiService = retrofit.create(ApiService.class);

        Call<Company> call = apiService.getcompany(user_type, sharedPreferences_for_login.getString(comID, ""));

        call.enqueue(new Callback<Company>() {
            @Override
            public void onResponse(Call<Company> call, Response<Company> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().GetSuccess()) {
                        Glide.with(Updateprofile.this)
                                .load(Api.BASE_URL + response.body().getCompanydetail().get(0).getCompany_profileimage_url())
                                .thumbnail(0.5f)
                                .apply(RequestOptions.placeholderOf(R.drawable.profile_image)
                                        .error(R.drawable.login_icon)
                                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                                .into(profile_imageview);
                        filepath = "";
                        comname_et.setText(response.body().getCompanydetail().get(0).getCompany_name());
                        email_et.setText(response.body().getCompanydetail().get(0).getCompany_email());
                        mobile_et.setText(response.body().getCompanydetail().get(0).getCompany_mobile());
                        address_et.setText(response.body().getCompanydetail().get(0).getCompany_address());
                        state_sp.setSelection(state_adapter.getPosition(response.body().getCompanydetail().get(0).getCompany_state()));
                        city_sp.setSelection(city_adapter.getPosition(response.body().getCompanydetail().get(0).getCompany_city()));
                        website_et.setText(response.body().getCompanydetail().get(0).getCompany_website());
                    } else {
                        Toast.makeText(Updateprofile.this, response.body().GetMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Updateprofile.this, response.body().GetMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Company> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Updateprofile.this, "failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQUEST_CODE && resultCode==RESULT_OK)
        {
            Uri uri=data.getData();
            try {
                Bitmap bitmap= MediaStore.Images.Media.getBitmap(Updateprofile.this.getContentResolver(),uri);
                profile_imageview.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            filepath=getRealPathFromURIPathString(uri,Updateprofile.this);
        }
    }

    public void setstate() {

        progressDialog = new ProgressDialog(Updateprofile.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        ApiService service = retrofit.create(ApiService.class);

        Call<State> call = service.getstate(user_type);

        call.enqueue(new Callback<State>() {
            @Override
            public void onResponse(Call<State> call, Response<State> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        strings_state.clear();
                        strings_state.add("Select State");
                        for (int i = 0; i < response.body().getStates().size(); i++) {
                            strings_state.add(response.body().getStates().get(i).getState_name());
                        }
                        state_adapter = new ArrayAdapter<String>(Updateprofile.this, R.layout.spinnn, strings_state);
                        state_sp.setAdapter(state_adapter);
                        state_adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(Updateprofile.this, "no data", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Updateprofile.this, "body null", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<State> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Updateprofile.this, "no response", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setcity(String state_name) {

        progressDialog = new ProgressDialog(Updateprofile.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

//        Log.d("test.registration.","is.."+state_name);

        ApiService service = retrofit.create(ApiService.class);

        Call<City> call = service.getcity(state_name);

        call.enqueue(new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        if (response.body().getCities() != null) {
                            strings_city.clear();
                            strings_city.add("Select City");
                            for (int i = 0; i < response.body().getCities().size(); i++) {
                                strings_city.add(response.body().getCities().get(i).getCity_name());
                                Log.d("Test..1", "city_array.11." + response.body().getCities().get(i).getCity_name());
                            }

                            Log.d("Test..1", "city_array.." + strings_city);
                            city_adapter = new ArrayAdapter<String>(Updateprofile.this, R.layout.spinnn, strings_city);
                            city_sp.setAdapter(city_adapter);
                            city_adapter.notifyDataSetChanged();
                            set_company_details();

                        } else {
                            Toast.makeText(Updateprofile.this, "cities are null", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Updateprofile.this, "no data", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Updateprofile.this, "body null", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Updateprofile.this, "no response", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String getRealPathFromURIPathString(Uri uri, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);

        if (cursor == null) {
            return uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public void updatedata(String user_type, String comID, String filepath, String cname, String email, String mobile, String address, String state, String city,String website) {
        Log.d("joshivishal", "usertype>>" + user_type);
        Log.d("joshivishal", "filepath1123...> " + filepath);
        if (user_type != null && comID != null && cname != null && email != null &&
                mobile != null && address != null && state != null && city != null && website != null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.level(HttpLoggingInterceptor.Level.BASIC);
            interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
            interceptor.level(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(360, TimeUnit.MINUTES)
                    .readTimeout(360, TimeUnit.MINUTES)
                    .writeTimeout(360, TimeUnit.MINUTES)
                    .addInterceptor(interceptor)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(Api.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            ApiService service = retrofit.create(ApiService.class);

//            MultipartBody.Builder builder = new MultipartBody.Builder();
//            builder.addFormDataPart("user_type", user_type);
//            builder.addFormDataPart("studID", studID);
//            builder.addFormDataPart("fname", fname);
//            builder.addFormDataPart("gender", gender);
//            builder.addFormDataPart("email", email);
//            builder.addFormDataPart("mobile", mobile);
//            builder.addFormDataPart("address", address);
//            builder.addFormDataPart("state", state);
//            builder.addFormDataPart("city", city);
//            builder.addFormDataPart("college", college);
//            builder.addFormDataPart("branch", branch);
//            builder.addFormDataPart("sem", sem);
//            builder.addFormDataPart("enrollment", enrollment);
//            MultipartBody requestBody = builder.build();
            MultipartBody.Part body;

            if (!filepath.isEmpty()) {
                File file = new File(filepath);

                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), file);
                body =
                        MultipartBody.Part.createFormData("image", file.getName(), requestFile);

            } else {
                body = null;
            }


// add another part within the multipart request
            RequestBody user_type_1 =
                    RequestBody.create(MediaType.parse("multipart/form-data"), user_type);
            RequestBody studID_1 =
                    RequestBody.create(MediaType.parse("multipart/form-data"), comID);
            RequestBody cname_1 =
                    RequestBody.create(MediaType.parse("multipart/form-data"), cname);
            RequestBody email_1 =
                    RequestBody.create(MediaType.parse("multipart/form-data"), email);
            RequestBody mobile_1 =
                    RequestBody.create(MediaType.parse("multipart/form-data"), mobile);
            RequestBody address_1 =
                    RequestBody.create(MediaType.parse("multipart/form-data"), address);
            RequestBody state_1 =
                    RequestBody.create(MediaType.parse("multipart/form-data"), state);
            RequestBody city_1 =
                    RequestBody.create(MediaType.parse("multipart/form-data"), city);
            RequestBody website_1 =
                    RequestBody.create(MediaType.parse("multipart/form-data"), website);




//            Log.e(TAG, "updatedata: ", );

            Call<Result> call = service.updateprofile(user_type_1,studID_1,cname_1,email_1,mobile_1,address_1
                    ,state_1,city_1,website_1,body);

            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor2 = sharedPreferences_for_login.edit();
                            editor2.putString(comemail, email_et.getText().toString());
                            editor2.commit();
                            update_data_result_tv.setText("Data Updated Successfully!!");
                        } else {
                            Toast.makeText(Updateprofile.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Updateprofile.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    Toast.makeText(Updateprofile.this, "no response", Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(this, "null", Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
