package com.example.crcompany;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.crcompany.APIs.Api;
import com.example.crcompany.APIs.ApiService;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StudentDetails extends AppCompatActivity {
    Button photodownload_btn,resumedowmload_btn;
    TextView studname_tv,gender_tv,semail_tv,smobile_tv,collegename_tv,branch_tv,sem_tv,enroll_num_tv,cgpa_tv,applieddate_tv,photodownload_tv,resumedownload_tv;

    String myprefe2 = "login_shared_pre", comID = "ID", comemail = "Email", compass = "Password", user_type = "company", cname, mobile, email, address, state, city, website;
    SharedPreferences sharedPreferences_for_login;
    String TAG  = "StudentDetails";
    String PHOTOPATH,PHOTONAME,FILEPATH,FILENAME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        studname_tv=findViewById(R.id.student_details_stud_name_tv);
        gender_tv=findViewById(R.id.student_details_gender_tv);
        semail_tv=findViewById(R.id.student_details_stud_email_tv);
        smobile_tv=findViewById(R.id.student_details_stud_mobile_tv);
        collegename_tv=findViewById(R.id.student_details_college_tv);
        branch_tv=findViewById(R.id.student_details_branch_tv);
        sem_tv=findViewById(R.id.student_details_sem_tv);
        enroll_num_tv=findViewById(R.id.student_details_enrollment_tv);
        cgpa_tv=findViewById(R.id.student_details_cgpa_tv);
        applieddate_tv=findViewById(R.id.student_details_applied_date_tv);
        photodownload_btn=findViewById(R.id.student_details_download_photo_btn);
        resumedowmload_btn=findViewById(R.id.student_details_download_resume_btn);
        photodownload_tv=findViewById(R.id.photo_download_tv);
        resumedownload_tv=findViewById(R.id.resume_download_tv);

        studname_tv.setText(getIntent().getStringExtra("stud_app_stud_name"));
        gender_tv.setText(getIntent().getStringExtra("stud_app_stud_gender"));
        semail_tv.setText(getIntent().getStringExtra("stud_app_stud_email"));
        smobile_tv.setText(getIntent().getStringExtra("stud_app_stud_mobile"));
        collegename_tv.setText(getIntent().getStringExtra("stud_app_stud_clgname"));
        branch_tv.setText(getIntent().getStringExtra("stud_app_stud_branch"));
        sem_tv.setText(getIntent().getStringExtra("stud_app_stud_sem"));
        enroll_num_tv.setText(getIntent().getStringExtra("stud_app_stud_enrollment"));
        cgpa_tv.setText(getIntent().getStringExtra("stud_app_stud_cgpa"));
        applieddate_tv.setText(getIntent().getStringExtra("stud_app_stud_applieddate"));
    }


    public void onclick3(View view) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("pelase wait!!");
        progressDialog.show();


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(360, TimeUnit.MINUTES)
                .writeTimeout(360, TimeUnit.MINUTES)
                .readTimeout(360, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        ApiService apiService = retrofit.create(ApiService.class);
        String tmp=getIntent().getStringExtra("stud_app_stud_photourl");
        PHOTONAME=tmp.substring(8);
        Log.d("Joshivishal", "onclick3: "+PHOTONAME);
        PHOTOPATH="https://cheering-numerals.000webhostapp.com/CampusRecruitment/"+getIntent().getStringExtra("stud_app_stud_photourl");
//            Call<ResponseBody> call=apiService.downloadFileWithDynamicUrlAsync();
        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrlAsync(PHOTOPATH);
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "server contacted and has file");

                    boolean writtenToDisk = writeResponseBodyToDisk(response.body(),PHOTONAME);

                    if (writtenToDisk)
                    {
                        photodownload_tv.setVisibility(View.VISIBLE);
                        photodownload_tv.setText(PHOTONAME+" Downloaded Successfully!!");
                    }
                    else
                    {
                        photodownload_tv.setVisibility(View.VISIBLE);
                        photodownload_tv.setText(" Try Again!!");
                        photodownload_tv.setTextColor(R.color.red);
                    }

                    Log.d(TAG, "file download was a success? " + writtenToDisk);
                    progressDialog.dismiss();
                } else {
                    Log.d(TAG, "server contact failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Log.e(TAG, "error");
            }
        });
    }

    public void onclick4(View view) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("pelase wait!!");
        progressDialog.show();


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(360, TimeUnit.MINUTES)
                .writeTimeout(360, TimeUnit.MINUTES)
                .readTimeout(360, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        ApiService apiService = retrofit.create(ApiService.class);

        String tmp=getIntent().getStringExtra("stud_app_stud_fileurl");
        FILENAME=tmp.substring(8);
        Log.d("Joshivishal", "onclick3: "+PHOTONAME);
        FILEPATH="https://cheering-numerals.000webhostapp.com/CampusRecruitment/"+getIntent().getStringExtra("stud_app_stud_fileurl");

//            Call<ResponseBody> call=apiService.downloadFileWithDynamicUrlAsync();
        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrlAsync(FILEPATH);
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "server contacted and has file");

                    boolean writtenToDisk = writeResponseBodyToDisk(response.body(),FILENAME);
                    if (writtenToDisk)
                    {
                        resumedownload_tv.setVisibility(View.VISIBLE);
                        resumedownload_tv.setText(FILENAME+" Downloaded Successfully!!");
                    }
                    else
                    {
                        resumedownload_tv.setVisibility(View.VISIBLE);
                        resumedownload_tv.setText("Try Again!!");
                        resumedownload_tv.setTextColor(R.color.red);
                    }

                    Log.d(TAG, "file download was a success? " + writtenToDisk);
                    progressDialog.dismiss();
                } else {
                    Log.d(TAG, "server contact failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Log.e(TAG, "error");
            }
        });
    }

    private boolean writeResponseBodyToDisk(ResponseBody body,String file_name) {
        try {
            // todo change the file location/name according to your needs
            File futureStudioIconFile = new File(getExternalFilesDir(null) + File.separator + file_name);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Toast.makeText(this, file_name+" downloaded successfully" , Toast.LENGTH_SHORT).show();
//                    Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
