package com.example.crcompany;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.crcompany.APIs.Api;
import com.example.crcompany.APIs.ApiService;
import com.example.crcompany.ModelClass.Result;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Makerequest extends AppCompatActivity {

    ImageView imageView;
    EditText date_et, email_et, mobile_et, website_et, otherdetails_et;
    Spinner placeofinterview_sp, language_sp;
    TextView email_tv, interview_date_tv;
    Button request_btn;
    ArrayList<String> places = new ArrayList<>();
    ArrayList<String> languages = new ArrayList<>();

    SharedPreferences sharedPreferences_for_login;
    String typedemail, emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String myprefe2 = "login_shared_pre", comID = "ID", comemail = "Email", compass = "Password", comname = "comname", user_type = "company",
            place, requested_date, inter_date, email, mobile, language, site, otherdetails;

    private int mYear;
    private int mMonth;
    private int mDay;
    private int mHour;
    private int mMinute;
    private Calendar c;
    private Context ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makerequest);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imageView = findViewById(R.id.makereq_imageview_logo);
        placeofinterview_sp = findViewById(R.id.makereq_placeofinterview_sp);
        interview_date_tv = findViewById(R.id.makereq_datepicker);
        email_et = findViewById(R.id.makereq_email);
        email_tv = findViewById(R.id.makereq_Email_tv);
        mobile_et = findViewById(R.id.makereq_mobile);
        language_sp = findViewById(R.id.makereq_language_sp);
        website_et = findViewById(R.id.makereq_com_website);
        otherdetails_et = findViewById(R.id.makereq_otherdetails);
        request_btn = findViewById(R.id.makereq_request_btn);


        places.add("Select Place");
        places.add("AT COMPANY");
        places.add("AT COLLEGE");

        final ArrayAdapter<String> placesadapter = new ArrayAdapter<String>(this, R.layout.spinnn, places);
        placeofinterview_sp.setAdapter(placesadapter);

        languages.add("Select Languages");

        languages.add("Android");
        languages.add("IOS");
        languages.add("Hybrid");
        languages.add("PHP");
        languages.add("DOT NET");
        languages.add("Python");


        final ArrayAdapter<String> languageadapter = new ArrayAdapter<String>(this, R.layout.spinnn, languages);
        language_sp.setAdapter(languageadapter);

        interview_date_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_Datepicker();
            }
        });

        email_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                typedemail = email_et.getText().toString().trim();
                if (typedemail.matches(emailPattern) && editable.length() > 0) {

                    email_tv.setText("valid Email");
                    email_tv.setTextColor(getResources().getColor(R.color.green));
                } else {

                    email_tv.setText("invalid Email");
                    email_tv.setTextColor(getResources().getColor(R.color.red));
                }

            }
        });


    }

    public void onclick(View view) {
        if (placeofinterview_sp.getSelectedItemPosition() == 0) {
            Toast.makeText(Makerequest.this, "Select Interview Place!!", Toast.LENGTH_SHORT).show();
        } else if (interview_date_tv.getText().toString().isEmpty()) {
            interview_date_tv.setError("select date here!!");
        } else if (email_et.length() == 0) {
            email_et.setError("enter email");
        } else if (email_tv.getText().toString().equals("invalid Email")) {
            email_et.setError("Enter Valid Email address");
        } else if (mobile_et.getText().toString().isEmpty()) {
            mobile_et.setError("Enter Mobile no.!!");
        } else if (language_sp.getSelectedItemPosition() == 0) {
            Toast.makeText(Makerequest.this, "Select Language here!!!", Toast.LENGTH_SHORT).show();
        } else if (website_et.getText().toString().isEmpty()) {
            website_et.setError("Enter Website here!!!");
        } else if (otherdetails_et.getText().toString().isEmpty()) {
            otherdetails_et.setError("Enter other Details here!!!");
        } else {
            final ProgressDialog progressDialog = new ProgressDialog(Makerequest.this);
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            place = placeofinterview_sp.getSelectedItem().toString();
            inter_date = interview_date_tv.getText().toString();
            email = email_et.getText().toString();
            mobile = mobile_et.getText().toString();
            language = language_sp.getSelectedItem().toString();
            site = website_et.getText().toString();
            otherdetails = otherdetails_et.getText().toString();

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
            requested_date = sdf.format(new Date());


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Api.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            ApiService service = retrofit.create(ApiService.class);

            Call<Result> call = service.comMakereq(sharedPreferences_for_login.getString(comID, ""), sharedPreferences_for_login.getString(comname, ""), place, requested_date, inter_date, email, mobile, language, site, otherdetails);

            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    progressDialog.dismiss();
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            Intent intent = new Intent(Makerequest.this, Requests.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(Makerequest.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Makerequest.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(Makerequest.this, "no response", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    private void show_Datepicker() {

        mYear = Calendar.getInstance().get(Calendar.YEAR);
        mMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
        mDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        mHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        mMinute = Calendar.getInstance().get(Calendar.MINUTE);

        c = Calendar.getInstance();
        int mYearParam = mYear;
        int mMonthParam = mMonth - 1;
        int mDayParam = mDay;

        DatePickerDialog datePickerDialog = new DatePickerDialog(ctx,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        mMonth = monthOfYear + 1;
                        mYear = year;
                        mDay = dayOfMonth;

                        interview_date_tv.setText(mDay + "-" + mMonth + "-" + mYear);
                    }


                }, mYearParam, mMonthParam, mDayParam);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        datePickerDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2 = sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent = new Intent(this, Login.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
