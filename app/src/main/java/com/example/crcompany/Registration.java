package com.example.crcompany;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import com.example.crcompany.APIs.Api;
import com.example.crcompany.APIs.ApiService;
import com.example.crcompany.ModelClass.City;
import com.example.crcompany.ModelClass.Result;
import com.example.crcompany.ModelClass.State;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Registration extends AppCompatActivity {

    CircleImageView profile_imageview;
    EditText comname_et, email_et, mobile_et, address_et, state_et, password_et, confirm_pass_et, website_et;
    Button signup_btn;
    TextView email_tv;
    Spinner city_sp, state_sp;
    ProgressDialog progressDialog;
    Cursor cursor;


    SharedPreferences sharedPreferences_for_login;
    String typedemail, emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    Integer REQUEST_CODE = 200;
    ArrayList<String> strings_city = new ArrayList<>();
    ArrayList<String> strings_state = new ArrayList<>();

    ArrayAdapter<String> city_adapter;
    ArrayAdapter<String> state_adapter;

    String filepath;
    String myprefe2 = "login_shared_pre", comID = "ID", comemail = "Email", compass = "Password", comname = "comname", user_type = "company", cname, mobile, email, address, state, city, website, password;

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Api.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        profile_imageview = findViewById(R.id.registration_profile_image);
        profile_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE);
            }
        });


        comname_et = findViewById(R.id.registration_com_name);
        email_et = findViewById(R.id.registration_email);
        email_tv = findViewById(R.id.regist_Email_tv);
        mobile_et = findViewById(R.id.registration_mobile);
        address_et = findViewById(R.id.registration_address);
        state_sp = findViewById(R.id.registration_state_sp);
        city_sp = findViewById(R.id.registration_city_sp);
        website_et = findViewById(R.id.registration_website);
        password_et = findViewById(R.id.registration_password);
        confirm_pass_et = findViewById(R.id.registration_confirm_password);
        signup_btn = findViewById(R.id.registration_signup_btn);

        strings_state.add("Select State");
        strings_city.add("Select City");

        setstate();
        state_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setcity(state_sp.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                setcity(state_sp.getSelectedItem().toString());
            }
        });

        email_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                typedemail = email_et.getText().toString().trim();
                if (typedemail.matches(emailPattern) && editable.length() > 0) {

                    email_tv.setText("valid Email");
                    email_tv.setTextColor(getResources().getColor(R.color.green));
                } else {

                    email_tv.setText("invalid Email");
                    email_tv.setTextColor(getResources().getColor(R.color.red));
                }

            }
        });
    }

    public void onclick1(View v) {
        if (comname_et.length() == 0) {
            comname_et.setError("enter fullname");
        } else if (email_et.length() == 0) {
            email_et.setError("enter email");
        } else if (email_tv.getText().toString().equals("invalid Email")) {
            email_et.setError("Enter Valid Email address");
        } else if (mobile_et.length() < 10) {
            mobile_et.setError("enter mobile number");
        } else if (address_et.length() == 0) {
            address_et.setError("enter address");
        } else if (state_sp.getSelectedItemPosition() == 0) {
            Toast.makeText(Registration.this, "Select State", Toast.LENGTH_SHORT).show();
        } else if (city_sp.getSelectedItemPosition() == 0) {
            Toast.makeText(Registration.this, "Select City", Toast.LENGTH_SHORT).show();
        } else if (website_et.length() == 0) {
            website_et.setError("enter site");
        } else if (password_et.length() < 6) {
            password_et.setError("enter password");
        } else if (confirm_pass_et.length() < 6) {
            confirm_pass_et.setError("enter confirm password");
        } else {
            if (password_et.getText().toString().equals(confirm_pass_et.getText().toString())) {

//                progressDialog = new ProgressDialog(Registration.this);
//                progressDialog.setMessage("Please Wait");
//                progressDialog.setCancelable(false);
//                progressDialog.setCanceledOnTouchOutside(false);
//                progressDialog.show();

                cname = comname_et.getText().toString();
                email = email_et.getText().toString();
                mobile = mobile_et.getText().toString();
                address = address_et.getText().toString();
                state = state_sp.getSelectedItem().toString();
                city = city_sp.getSelectedItem().toString();
                website = website_et.getText().toString();
                password = password_et.getText().toString();

                Log.d("joshivishal", "filepath" + filepath + user_type + cname + email + mobile + address + state + city + website + password);
                if (filepath == null) {
                    Toast.makeText(this, "select profie picture", Toast.LENGTH_SHORT).show();
                } else {
                    uploadRegistrationData(filepath, user_type, cname, email, mobile, address, state, city, website, password);
                }

//                ApiService apiService = retrofit.create(ApiService.class);
//
//                Call<Result> call = apiService.comRegistration(user_type, cname, email, mobile, address, state, city, website, password);
//
//                call.enqueue(new Callback<Result>() {
//                    @Override
//                    public void onResponse(Call<Result> call, Response<Result> response) {
//                        progressDialog.dismiss();
//                        if (response.body() != null) {
//                            if (response.body().getSuccess()) {
//                                sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor2 = sharedPreferences_for_login.edit();
//                                editor2.putString(comemail, email_et.getText().toString());
//                                editor2.putString(compass, password_et.getText().toString());
//                                editor2.putString(comID, response.body().getID().toString());
//                                editor2.putString(comname, response.body().getCompany_name());
//                                editor2.commit();
//                                Intent intent = new Intent(Registration.this, Company_dashboard.class);
//                                startActivity(intent);
//                                finish();
//                            } else {
//                                Toast.makeText(Registration.this, "data is not submitted", Toast.LENGTH_SHORT).show();
//                            }
//                        } else {
//                            Toast.makeText(Registration.this, "no response", Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//
//                    @Override
//                    public void onFailure(Call<Result> call, Throwable t) {
//                        progressDialog.dismiss();
//                        Toast.makeText(Registration.this, "response failed", Toast.LENGTH_SHORT).show();
//                    }
//                });
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(Registration.this.getContentResolver(), uri);
                profile_imageview.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            filepath = getRealPathFromURIPathString(uri, Registration.this);
        }

    }

    public void setstate() {

        progressDialog = new ProgressDialog(Registration.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        ApiService service = retrofit.create(ApiService.class);

        Call<State> call = service.getstate(user_type);

        call.enqueue(new Callback<State>() {
            @Override
            public void onResponse(Call<State> call, Response<State> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        strings_state.clear();
                        strings_state.add("Select State");
                        for (int i = 0; i < response.body().getStates().size(); i++) {
                            strings_state.add(response.body().getStates().get(i).getState_name());
                        }
                        state_adapter = new ArrayAdapter<String>(Registration.this, R.layout.spinnn, strings_state);
                        state_sp.setAdapter(state_adapter);
                        state_adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(Registration.this, "no data", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Registration.this, "body null", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<State> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Registration.this, "no response", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setcity(String state_name) {

        progressDialog = new ProgressDialog(Registration.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

//        Log.d("test.registration.","is.."+state_name);

        ApiService service = retrofit.create(ApiService.class);

        Call<City> call = service.getcity(state_name);

        call.enqueue(new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        if (response.body().getCities() != null) {
                            strings_city.clear();
                            strings_city.add("Select City");
                            for (int i = 0; i < response.body().getCities().size(); i++) {
                                strings_city.add(response.body().getCities().get(i).getCity_name());
                                Log.d("Test..1", "city_array.11." + response.body().getCities().get(i).getCity_name());
                            }

                            Log.d("Test..1", "city_array.." + strings_city);
                            city_adapter = new ArrayAdapter<String>(Registration.this, R.layout.spinnn, strings_city);
                            city_sp.setAdapter(city_adapter);
                            city_adapter.notifyDataSetChanged();


                        } else {
                            Toast.makeText(Registration.this, "cities are null", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(Registration.this, "no data", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Registration.this, "body null", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(Registration.this, "no response", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getRealPathFromURIPathString(Uri uri, Activity activity) {
        Log.d("joshivishal", "uri>>>> " + uri);
        cursor = activity.getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
            return uri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public void uploadRegistrationData(String filepath, String user_type, String cname, final String email, String mobile, String address, String state, String city, String website, final String password) {

        progressDialog = new ProgressDialog(Registration.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BASIC);
        interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(360, TimeUnit.MINUTES)
                .readTimeout(360, TimeUnit.MINUTES)
                .writeTimeout(360, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        builder.addFormDataPart("user_type", user_type);
        builder.addFormDataPart("cname", cname);
        builder.addFormDataPart("email", email);
        builder.addFormDataPart("mobile", mobile);
        builder.addFormDataPart("address", address);
        builder.addFormDataPart("state", state);
        builder.addFormDataPart("city", city);
        builder.addFormDataPart("website", website);
        builder.addFormDataPart("password", password);

        Log.d("joshivishal", "filepath>>>> " + filepath);
        File file = new File(filepath);
        builder.addFormDataPart("profile_image", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));

        final MultipartBody requestBody = builder.build();

        Call<Result> call = service.studentRegistration(requestBody);

        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().getSuccess()) {
//                        sharedPreferences_for_login = getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor2 = sharedPreferences_for_login.edit();
//                        editor2.putString(comemail, email_et.getText().toString());
//                        editor2.putString(compass, password_et.getText().toString());
//                        editor2.putString(comID, response.body().getID().toString());
//                        editor2.putString(comname, response.body().getCompany_name());
//                        editor2.commit();
                        Intent intent = new Intent(Registration.this, Login.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(Registration.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(Registration.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("joshivishal", "is>>" + t.getMessage());
                Toast.makeText(Registration.this, "no response", Toast.LENGTH_SHORT).show();

            }
        });
    }


}

