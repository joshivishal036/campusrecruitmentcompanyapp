package com.example.crcompany;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.crcompany.APIs.Api;
import com.example.crcompany.APIs.ApiService;
import com.example.crcompany.Adapters.AppliedStudentsAdapter;
import com.example.crcompany.ModelClass.StudentApplications;
import com.example.crcompany.ModelClass.StudentApplicationsDetails;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RequestDetails extends AppCompatActivity implements AppliedStudentsAdapter.Adapterclick {

    TextView companyname_tv,lanuage_tv,requested_date_tv,interview_date_tv,email_tv,mobile_tv,place_of_interview_tv,website_tv,other_details_tv;
    SharedPreferences sharedPreferences_for_login;
    String myprefe2 = "login_shared_pre", comID = "ID",comemail="Email",compass="Password",comname="comname",user_type="company",email,password;
    RecyclerView recyclerView;
    ArrayList<StudentApplicationsDetails> student_details_array=new ArrayList<>();
    ProgressDialog progressDialog;
    AppliedStudentsAdapter appliedStudentsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        companyname_tv=findViewById(R.id.req_dtl_company_name_ans_tv);
        lanuage_tv=findViewById(R.id.req_dtl_language_ans_tv);
        requested_date_tv=findViewById(R.id.req_dtl_requested_date_ans_tv);
        interview_date_tv=findViewById(R.id.req_dtl_interview_date_ans_tv);
        email_tv=findViewById(R.id.req_dtl_email_ans_tv);
        mobile_tv=findViewById(R.id.req_dtl_mobile_ans_tv);
        place_of_interview_tv=findViewById(R.id.req_dtl_placeofInterview_ans_tv);
        website_tv=findViewById(R.id.req_dtl_website_ans_tv);
        other_details_tv=findViewById(R.id.req_dtl_other_details_ans_tv);

        recyclerView=findViewById(R.id.applied_stud_recyclerview);

        companyname_tv.setText(getIntent().getStringExtra("companyname"));
        lanuage_tv.setText(getIntent().getStringExtra("language"));
        requested_date_tv.setText(getIntent().getStringExtra("requested_date"));
        interview_date_tv.setText(getIntent().getStringExtra("interview_date"));
        email_tv.setText(getIntent().getStringExtra("email"));
        mobile_tv.setText(getIntent().getStringExtra("mobile"));
        place_of_interview_tv.setText(getIntent().getStringExtra("interview_place"));
        website_tv.setText(getIntent().getStringExtra("website"));
        other_details_tv.setText(getIntent().getStringExtra("otherdetails"));

        progressDialog = new ProgressDialog(RequestDetails.this);
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        HttpLoggingInterceptor interceptor=new HttpLoggingInterceptor();
                interceptor.level(HttpLoggingInterceptor.Level.BASIC);
                interceptor.level(HttpLoggingInterceptor.Level.BODY);
                interceptor.level(HttpLoggingInterceptor.Level.HEADERS);
        OkHttpClient okHttpClient=new OkHttpClient.Builder()
                .connectTimeout(360, TimeUnit.MINUTES)
                .readTimeout(360,TimeUnit.MINUTES)
                .writeTimeout(360,TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .build();

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
//        Toast.makeText(this, getIntent().getStringExtra("cr_id"), Toast.LENGTH_SHORT).show();
        ApiService service=retrofit.create(ApiService.class);
        Call<StudentApplications> call=service.getstudentapplication(user_type,getIntent().getStringExtra("cr_id"));

        call.enqueue(new Callback<StudentApplications>() {
            @Override
            public void onResponse(Call<StudentApplications> call, Response<StudentApplications> response) {
                progressDialog.dismiss();
                if (response.body()!=null)
                {
                    if (response.body().isSuccess())
                    {
                        student_details_array.clear();
                        student_details_array.addAll(response.body().getStudentApplicationsDetails());
                        appliedStudentsAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        Toast.makeText(RequestDetails.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(RequestDetails.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<StudentApplications> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(RequestDetails.this, "no response", Toast.LENGTH_SHORT).show();
            }
        });
        appliedStudentsAdapter=new AppliedStudentsAdapter(RequestDetails.this,student_details_array);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(RequestDetails.this));
        recyclerView.setAdapter(appliedStudentsAdapter);
        appliedStudentsAdapter.notifyDataSetChanged();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(RequestDetails.this,Makerequest.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void recyclerviewclick(int position) {
        Intent intent=new Intent(RequestDetails.this,StudentDetails.class);
        intent.putExtra("stud_app_stud_id",student_details_array.get(position).getStud_app_stud_id());
        intent.putExtra("stud_app_stud_name",student_details_array.get(position).getStud_app_stud_name());
        intent.putExtra("stud_app_stud_gender",student_details_array.get(position).getStud_app_stud_gender());
        intent.putExtra("stud_app_stud_email",student_details_array.get(position).getStud_app_stud_email());
        intent.putExtra("stud_app_stud_mobile",student_details_array.get(position).getStud_app_stud_mobile());
        intent.putExtra("stud_app_stud_clgname",student_details_array.get(position).getStud_app_stud_clgname());
        intent.putExtra("stud_app_stud_branch",student_details_array.get(position).getStud_app_stud_branch());
        intent.putExtra("stud_app_stud_sem",student_details_array.get(position).getStud_app_stud_sem());
        intent.putExtra("stud_app_stud_enrollment",student_details_array.get(position).getStud_app_stud_enrollment());
        intent.putExtra("stud_app_stud_cgpa",student_details_array.get(position).getStud_app_stud_cgpa());
        intent.putExtra("stud_app_stud_applieddate",student_details_array.get(position).getStud_app_stud_applieddate());
        intent.putExtra("stud_app_stud_photourl",student_details_array.get(position).getStud_app_stud_photourl());
        intent.putExtra("stud_app_stud_fileurl",student_details_array.get(position).getStud_app_stud_fileurl());
        startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
