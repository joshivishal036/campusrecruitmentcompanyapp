package com.example.crcompany;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.crcompany.APIs.Api;
import com.example.crcompany.APIs.ApiService;
import com.example.crcompany.Adapters.UpcomingRequestRecyclerAdapter;
import com.example.crcompany.ModelClass.GetRequestsSub;
import com.example.crcompany.ModelClass.Getrequests;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FragmentUpcomingRequests extends Fragment implements UpcomingRequestRecyclerAdapter.Adapterclick {

    RecyclerView req_recyclerView;
    ArrayList<GetRequestsSub> request_details_array = new ArrayList<>();
    UpcomingRequestRecyclerAdapter upcomingRequestRecyclerAdapter;
    SharedPreferences sharedPreferences_for_login;
    String myprefe2 = "login_shared_pre", comID = "ID",user_type="company";

    public FragmentUpcomingRequests() {
        // Required empty public constructor
    }
    public FragmentUpcomingRequests(  SharedPreferences sharedPreferences_for_login) {
        // Required empty public constructor
        this.sharedPreferences_for_login=sharedPreferences_for_login;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root= inflater.inflate(R.layout.fragment_upcoming_requests, container, false);

        req_recyclerView = root.findViewById(R.id.upcoming_request_recycler_view);
        final ProgressDialog progressDialog = new ProgressDialog(getContext());

        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService service = retrofit.create(ApiService.class);
        Call<Getrequests> call = service.comGetreqs(user_type,sharedPreferences_for_login.getString(comID, ""));

        call.enqueue(new Callback<Getrequests>() {
            @Override
            public void onResponse(Call<Getrequests> call, Response<Getrequests> response) {
                progressDialog.dismiss();
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        request_details_array.clear();
                        request_details_array.addAll(response.body().getUpcoming_requests_details());
                        upcomingRequestRecyclerAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Getrequests> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), "no response", Toast.LENGTH_SHORT).show();
                Log.d("Requestdd", "iss>>" + t.getMessage());
            }
        });


        upcomingRequestRecyclerAdapter = new UpcomingRequestRecyclerAdapter(FragmentUpcomingRequests.this, request_details_array);

        req_recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        req_recyclerView.setHasFixedSize(true);
        req_recyclerView.setAdapter(upcomingRequestRecyclerAdapter);



        return root;
    }

    @Override
    public void recyclerviewclick(int position) {

        Intent intent = new Intent(getContext(), RequestDetails.class);
        intent.putExtra("cr_id", request_details_array.get(position).getCr_id());
        intent.putExtra("companyname", request_details_array.get(position).getCr_company_name());
        intent.putExtra("language", request_details_array.get(position).getCr_language());
        intent.putExtra("requested_date", request_details_array.get(position).getCr_requested_date());
        intent.putExtra("interview_date", request_details_array.get(position).getCr_interview_date());
        intent.putExtra("email", request_details_array.get(position).getCr_email());
        intent.putExtra("mobile", request_details_array.get(position).getCr_mobile());
        intent.putExtra("interview_place", request_details_array.get(position).getCr_interview_place());
        intent.putExtra("website", request_details_array.get(position).getCr_website());
        intent.putExtra("otherdetails", request_details_array.get(position).getCr_otherdetails());
        startActivity(intent);

    }
}
