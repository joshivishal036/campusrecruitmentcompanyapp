package com.example.crcompany;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.crcompany.Adapters.ComDashRecyclerAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Company_dashboard extends AppCompatActivity implements ComDashRecyclerAdapter.Adapterclick{

    RecyclerView dashboard_recyclerview;
    ArrayList<String> item_name=new ArrayList<>();
    ArrayList<Integer> icon_image=new ArrayList<>();
    ComDashRecyclerAdapter comDashRecyclerAdapter=new ComDashRecyclerAdapter(Company_dashboard.this,icon_image,item_name);
    String myprefe2 = "login_shared_pre", comID = "ID", comemail = "Email", compass = "Password", user_type = "company", cname, mobile, email, address, state, city, website;
    SharedPreferences sharedPreferences_for_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_dashboard);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dashboard_recyclerview=findViewById(R.id.company_dashboard_recyclerview);
        item_name.add("Update profile");
        item_name.add("Make Request");
        item_name.add("Requests");
        item_name.add("About us");
        item_name.add("Contact us");
        icon_image.add(R.drawable.stud_dash_updatepro_icon);
        icon_image.add(R.drawable.stud_dash_companies_icon);
        icon_image.add(R.drawable.stud_dash_applications_icon);
        icon_image.add(R.drawable.stud_dash_aboutus_icon);
        icon_image.add(R.drawable.stud_dash_contactus_icon);

        dashboard_recyclerview.setHasFixedSize(true);
        dashboard_recyclerview.setLayoutManager(new GridLayoutManager(this, 2));
        dashboard_recyclerview.setAdapter(comDashRecyclerAdapter);

    }

    @Override
    public void recyclerviewclick(int position) {
        switch (position){
            case 0:
                Intent intent0=new Intent(Company_dashboard.this, Updateprofile.class);
                startActivity(intent0);
                break;
            case 1:
                Intent intent1=new Intent(Company_dashboard.this, Makerequest.class);
                startActivity(intent1);
                break;
            case 2:
                Intent intent2=new Intent(Company_dashboard.this, Requests.class);
                startActivity(intent2);
                break;
            case 3:
                Intent intent4=new Intent(Company_dashboard.this, Aboutus.class);
                startActivity(intent4);
                break;
            case 4:
                Intent intent5=new Intent(Company_dashboard.this, Contactus.class);
                startActivity(intent5);
                break;

        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.action_settings)
        {
            sharedPreferences_for_login=getSharedPreferences(myprefe2, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor2=sharedPreferences_for_login.edit();
            editor2.clear();
            editor2.apply();
            editor2.commit();
            Intent intent=new Intent(this,Login.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}

