package com.example.crcompany.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class City {

    @SerializedName("success")
    private boolean success;

    @SerializedName("cities")
    private ArrayList<Cities> cities;

    public City(boolean success, ArrayList<Cities> cities) {
        this.success = success;
        this.cities = cities;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<Cities> getCities() {
        return cities;
    }

    public void setCities(ArrayList<Cities> cities) {
        this.cities = cities;
    }
}
