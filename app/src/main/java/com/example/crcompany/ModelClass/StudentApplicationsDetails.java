package com.example.crcompany.ModelClass;

import com.google.gson.annotations.SerializedName;

public class StudentApplicationsDetails {

    @SerializedName("stud_app_id")
    private int stud_app_id;

    @SerializedName("stud_app_stud_id")
    private int stud_app_stud_id;

    @SerializedName("stud_app_cr_id")
    private int stud_app_cr_id;

    @SerializedName("stud_app_stud_profileimage_url")
    private String stud_app_stud_profileimage_url;

    @SerializedName("stud_app_stud_name")
    private String stud_app_stud_name;

    @SerializedName("stud_app_stud_gender")
    private String stud_app_stud_gender;

    @SerializedName("stud_app_stud_email")
    private String stud_app_stud_email;

    @SerializedName("stud_app_stud_mobile")
    private String stud_app_stud_mobile;

    @SerializedName("stud_app_stud_state")
    private String stud_app_stud_state;

    @SerializedName("stud_app_stud_city")
    private String stud_app_stud_city;

    @SerializedName("stud_app_stud_clgname")
    private String stud_app_stud_clgname;

    @SerializedName("stud_app_stud_branch")
    private String stud_app_stud_branch;

    @SerializedName("stud_app_stud_sem")
    private String stud_app_stud_sem;

    @SerializedName("stud_app_stud_enrollment")
    private String stud_app_stud_enrollment;

    @SerializedName("stud_app_stud_cgpa")
    private String stud_app_stud_cgpa;

    @SerializedName("stud_app_stud_applieddate")
    private String stud_app_stud_applieddate;

    @SerializedName("stud_app_stud_photourl")
    private String stud_app_stud_photourl;

    @SerializedName("stud_app_stud_fileurl")
    private String stud_app_stud_fileurl;

    @SerializedName("stud_app_com_name")
    private String stud_app_com_name;

    @SerializedName("stud_app_com_language")
    private String stud_app_com_language;

    @SerializedName("stud_app_com_interdate")
    private String stud_app_com_interdate;

    @SerializedName("stud_app_com_email")
    private String stud_app_com_email;

    @SerializedName("stud_app_com_mobile")
    private String stud_app_com_mobile;

    @SerializedName("stud_app_com_interplace")
    private String stud_app_com_interplace;

    @SerializedName("stud_app_com_website")
    private String stud_app_com_website;

    @SerializedName("stud_app_com_otherdetails")
    private String stud_app_com_otherdetails;

    public StudentApplicationsDetails(int stud_app_id, int stud_app_stud_id, int stud_app_cr_id, String stud_app_stud_profileimage_url, String stud_app_stud_name, String stud_app_stud_gender, String stud_app_stud_email, String stud_app_stud_mobile, String stud_app_stud_state, String stud_app_stud_city, String stud_app_stud_clgname, String stud_app_stud_branch, String stud_app_stud_sem, String stud_app_stud_enrollment, String stud_app_stud_cgpa, String stud_app_stud_applieddate, String stud_app_stud_photourl, String stud_app_stud_fileurl, String stud_app_com_name, String stud_app_com_language, String stud_app_com_interdate, String stud_app_com_email, String stud_app_com_mobile, String stud_app_com_interplace, String stud_app_com_website, String stud_app_com_otherdetails) {
        this.stud_app_id = stud_app_id;
        this.stud_app_stud_id = stud_app_stud_id;
        this.stud_app_cr_id = stud_app_cr_id;
        this.stud_app_stud_profileimage_url = stud_app_stud_profileimage_url;
        this.stud_app_stud_name = stud_app_stud_name;
        this.stud_app_stud_gender = stud_app_stud_gender;
        this.stud_app_stud_email = stud_app_stud_email;
        this.stud_app_stud_mobile = stud_app_stud_mobile;
        this.stud_app_stud_state = stud_app_stud_state;
        this.stud_app_stud_city = stud_app_stud_city;
        this.stud_app_stud_clgname = stud_app_stud_clgname;
        this.stud_app_stud_branch = stud_app_stud_branch;
        this.stud_app_stud_sem = stud_app_stud_sem;
        this.stud_app_stud_enrollment = stud_app_stud_enrollment;
        this.stud_app_stud_cgpa = stud_app_stud_cgpa;
        this.stud_app_stud_applieddate = stud_app_stud_applieddate;
        this.stud_app_stud_photourl = stud_app_stud_photourl;
        this.stud_app_stud_fileurl = stud_app_stud_fileurl;
        this.stud_app_com_name = stud_app_com_name;
        this.stud_app_com_language = stud_app_com_language;
        this.stud_app_com_interdate = stud_app_com_interdate;
        this.stud_app_com_email = stud_app_com_email;
        this.stud_app_com_mobile = stud_app_com_mobile;
        this.stud_app_com_interplace = stud_app_com_interplace;
        this.stud_app_com_website = stud_app_com_website;
        this.stud_app_com_otherdetails = stud_app_com_otherdetails;
    }

    public int getStud_app_id() {
        return stud_app_id;
    }

    public int getStud_app_stud_id() {
        return stud_app_stud_id;
    }

    public int getStud_app_cr_id() {
        return stud_app_cr_id;
    }

    public String getStud_app_stud_profileimage_url() {
        return stud_app_stud_profileimage_url;
    }

    public String getStud_app_stud_name() {
        return stud_app_stud_name;
    }

    public String getStud_app_stud_gender() {
        return stud_app_stud_gender;
    }

    public String getStud_app_stud_email() {
        return stud_app_stud_email;
    }

    public String getStud_app_stud_mobile() {
        return stud_app_stud_mobile;
    }

    public String getStud_app_stud_state() {
        return stud_app_stud_state;
    }

    public String getStud_app_stud_city() {
        return stud_app_stud_city;
    }

    public String getStud_app_stud_clgname() {
        return stud_app_stud_clgname;
    }

    public String getStud_app_stud_branch() {
        return stud_app_stud_branch;
    }

    public String getStud_app_stud_sem() {
        return stud_app_stud_sem;
    }

    public String getStud_app_stud_enrollment() {
        return stud_app_stud_enrollment;
    }

    public String getStud_app_stud_cgpa() {
        return stud_app_stud_cgpa;
    }

    public String getStud_app_stud_applieddate() {
        return stud_app_stud_applieddate;
    }

    public String getStud_app_stud_photourl() {
        return stud_app_stud_photourl;
    }

    public String getStud_app_stud_fileurl() {
        return stud_app_stud_fileurl;
    }

    public String getStud_app_com_name() {
        return stud_app_com_name;
    }

    public String getStud_app_com_language() {
        return stud_app_com_language;
    }

    public String getStud_app_com_interdate() {
        return stud_app_com_interdate;
    }

    public String getStud_app_com_email() {
        return stud_app_com_email;
    }

    public String getStud_app_com_mobile() {
        return stud_app_com_mobile;
    }

    public String getStud_app_com_interplace() {
        return stud_app_com_interplace;
    }

    public String getStud_app_com_website() {
        return stud_app_com_website;
    }

    public String getStud_app_com_otherdetails() {
        return stud_app_com_otherdetails;
    }
}

