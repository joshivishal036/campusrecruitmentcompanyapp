package com.example.crcompany.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Company {

    @SerializedName("success")
    private  boolean success;

    @SerializedName("msg")
    private  String msg;

    @SerializedName("companydetails")
    private ArrayList<Companydetail> companydetail;


    public Company(boolean success, String msg, ArrayList<Companydetail> companydetail) {
        this.success = success;
        this.msg = msg;
        this.companydetail = companydetail;
    }

    public boolean GetSuccess() {
        return success;
    }

    public String GetMsg() {
        return msg;
    }

    public ArrayList<Companydetail> getCompanydetail() {
        return companydetail;
    }
}
