package com.example.crcompany.ModelClass;

import com.google.gson.annotations.SerializedName;

public class Cities {

    @SerializedName("city_name")
    private String city_name;

    public Cities(String city_name) {
        this.city_name = city_name;
    }

    public String getCity_name() {
        return city_name;
    }
}
