package com.example.crcompany.ModelClass;

import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("success")
    private boolean success;
    @SerializedName("msg")
    private  String msg;
    @SerializedName("ID")
    private Integer ID;
    @SerializedName("company_name")
    private  String company_name;

    public Result(boolean success, String msg, Integer ID, String company_name) {
        this.success = success;
        this.msg = msg;
        this.ID = ID;
        this.company_name = company_name;
    }

    public String getMsg() {

        return msg;
    }

    public Integer getID() {

        return ID;
    }

    public boolean getSuccess() {

        return success;
    }

    public String getCompany_name() {
        return company_name;
    }
}