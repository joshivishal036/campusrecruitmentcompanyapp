package com.example.crcompany.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class State {

    @SerializedName("success")
    private boolean success;

    @SerializedName("states")
    private ArrayList<States> states;

    public State(boolean success, ArrayList<States> states) {
        this.success = success;
        this.states = states;
    }

    public boolean isSuccess() {
        return success;
    }

    public ArrayList<States> getStates() {
        return states;
    }
}
