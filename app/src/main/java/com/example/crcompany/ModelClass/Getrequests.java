package com.example.crcompany.ModelClass;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Getrequests {
    @SerializedName("success")
    private boolean success;

    @SerializedName("msg")
    private String msg;

    @SerializedName("upcoming_requests_details")
    private ArrayList<GetRequestsSub> upcoming_requests_details;

    @SerializedName("outdated_requests_details")
    private ArrayList<GetRequestsSub> outdated_requests_details;

    public Getrequests(boolean success, String msg, ArrayList<GetRequestsSub> upcoming_requests_details, ArrayList<GetRequestsSub> outdated_requests_details) {
        this.success = success;
        this.msg = msg;
        this.upcoming_requests_details = upcoming_requests_details;
        this.outdated_requests_details = outdated_requests_details;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMsg() {
        return msg;
    }

    public ArrayList<GetRequestsSub> getUpcoming_requests_details() {
        return upcoming_requests_details;
    }

    public ArrayList<GetRequestsSub> getOutdated_requests_details() {
        return outdated_requests_details;
    }
}
