package com.example.crcompany.ModelClass;

import com.google.gson.annotations.SerializedName;

public class States {

    @SerializedName("state_name")
    private String state_name;

    public States(String state_name) {
        this.state_name = state_name;
    }

    public String getState_name() {
        return state_name;
    }
}
